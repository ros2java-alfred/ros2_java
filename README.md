ROS2 for Java (Alfred variation)
=============

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b0b714ef6e3f4dd88323ebad465e8dc0)](https://app.codacy.com/app/Alfred-Team/ros2_java?utm_source=github.com&utm_medium=referral&utm_content=ros2java-alfred/ros2_java&utm_campaign=badger)
[![Coverage Status](https://coveralls.io/repos/github/ros2java-alfred/ros2_java/badge.svg?branch=master)](https://coveralls.io/github/ros2java-alfred/ros2_java?branch=master) [![Documentation Status](https://readthedocs.org/projects/ros2-java-alfred/badge/?version=latest)](http://ros2-java-alfred.readthedocs.io/en/latest/?badge=latest) [![Apache 2 license](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://github.com/ros2java-alfred/ros2_java/blob/master/LICENSE) [![Chat Room](https://badges.gitter.im/gitterHQ/gitterHQ.github.io.svg)](https://gitter.im/ros2java-alfred?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) [![Coverity Status](https://img.shields.io/coverity/scan/10818.svg)](https://scan.coverity.com/projects/ros2java-alfred-ros2_java)

| Platform | Status |
|----------|--------|
| **OpenJDK** | [![Build Status](https://travis-ci.org/ros2java-alfred/ros2_java.svg?branch=master)](https://travis-ci.org/ros2java-alfred/ros2_java) |
| **Android** | [![Build Status](https://travis-ci.org/ros2java-alfred/ros2_android.svg?branch=master)](https://travis-ci.org/ros2java-alfred/ros2_android) |

What is this?
-------------

This is a set of projects (bindings, code generator, examples and more) that enables developers to write ROS2
applications for the JVM and Android with design of rosjava (ROS1).

Forked from [esteve/ros2_java](https://github.com/esteve/ros2_java) on December 27 2016

To start => [Wiki](https://github.com/ros2java-alfred/ros2_java/wiki)
